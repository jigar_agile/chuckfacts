package com.chuckfacts;

import java.util.ArrayList;
import com.chuckfacts.bean.JokeBean;
import android.app.Application;

public class MyApplication extends Application {

	public ArrayList<JokeBean> mArrayList;
	public ArrayList<JokeBean> favArrlist;
	private static MyApplication singleton;

	public static MyApplication getInstance() {
		return singleton;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
		mArrayList = new ArrayList<JokeBean>();
	}
}
