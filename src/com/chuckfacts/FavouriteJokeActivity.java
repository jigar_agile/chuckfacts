package com.chuckfacts;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import com.chuckfacts.listAdapter.FavouriteJokeAdapter;
import com.example.chuckfacts.R;

public class FavouriteJokeActivity extends Activity {
	
	private ListView lstFavorite;
	private FavouriteJokeAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favouritejoke);

		initialize();
	}

	private void initialize() {
		lstFavorite = (ListView) findViewById(R.id.lstFavorite);
		
		getActionBar().setTitle("Favorite Jokes");
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#516da8")));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		adapter = new FavouriteJokeAdapter(FavouriteJokeActivity.this);
		lstFavorite.setAdapter(adapter);
		lstFavorite.setEmptyView(findViewById(R.id.txtNoFav));
	}
	
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

	    int itemId = item.getItemId();
	    switch (itemId) {
	    case android.R.id.home:
	    	finish();
	        break;
	    }
	    return true;
	}
}
