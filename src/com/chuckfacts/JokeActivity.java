package com.chuckfacts;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;
import com.chuckfacts.bean.JokeBean;
import com.chuckfacts.callback.OnTaskCompletedListener;
import com.chuckfacts.common.FetchJokesAsync;
import com.chuckfacts.common.Utils;
import com.chuckfacts.listAdapter.JokeListAdapter;
import com.example.chuckfacts.R;

public class JokeActivity extends Activity implements OnScrollListener,
		OnTaskCompletedListener {

	private ListView lstJokes;
	private Boolean flagBoolean = false;
	private JokeListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);

		initialize();

		fetchJokes();
	}

	private void initialize() {
		lstJokes = (ListView) findViewById(R.id.lstJokes);
		getActionBar().setTitle("Jokes");
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#516da8")));

		adapter = new JokeListAdapter(JokeActivity.this);
		lstJokes.setAdapter(adapter);
		lstJokes.setOnScrollListener(this);
	}

	private void fetchJokes() {
		if (Utils.isNetworkAvailable(JokeActivity.this)) {
			new FetchJokesAsync(JokeActivity.this).execute();
		} else {
			Toast.makeText(JokeActivity.this,
					"Please Check Your Internet Connection.",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.favourite:
			setupFavData();
			startActivity(new Intent(JokeActivity.this,
					FavouriteJokeActivity.class));
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		int lastInScreen = firstVisibleItem + visibleItemCount;
		if (lastInScreen == totalItemCount && totalItemCount >= 10
				&& flagBoolean == false) {
			flagBoolean = true;
			fetchJokes();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void ontaskCompleted() {
		adapter.notifyDataSetChanged();

		if (flagBoolean != false)
			flagBoolean = false;
	}

	private void setupFavData() {
		MyApplication.getInstance().favArrlist = new ArrayList<JokeBean>();
		for (JokeBean b : MyApplication.getInstance().mArrayList) {
			if (b.isFavourite())
				MyApplication.getInstance().favArrlist.add(b);
		}
	}
}
