package com.chuckfacts.common;

import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import com.chuckfacts.MyApplication;
import com.chuckfacts.bean.JokeBean;
import com.chuckfacts.callback.OnTaskCompletedListener;

public class FetchJokesAsync extends AsyncTask<Void, Void, Void> {

	private Context mContext;
	private OnTaskCompletedListener taskComplted;
	private JSONObject mJsonObject;
	private JSONArray mJsonArray;

	public FetchJokesAsync(Context mContext) {
		this.mContext = mContext;
		taskComplted = (OnTaskCompletedListener) mContext;
		((Activity) mContext).setProgressBarIndeterminateVisibility(true);
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			String response = Utils
					.fetchJsonResponse("http://api.icndb.com/jokes/random/10");
			if (response != null) {
				mJsonObject = new JSONObject(response);
				mJsonArray = new JSONArray(mJsonObject.getString("value"));
				for (int i = 0; i < mJsonArray.length(); i++) {
					JokeBean bean = new JokeBean();
					bean.setJoke(mJsonArray.getJSONObject(i).getString("joke"));
					MyApplication.getInstance().mArrayList.add(bean);
				}
			}
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		((Activity) mContext).setProgressBarIndeterminateVisibility(false);
		taskComplted.ontaskCompleted();
	}
}
