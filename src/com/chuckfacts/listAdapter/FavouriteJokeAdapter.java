package com.chuckfacts.listAdapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.chuckfacts.MyApplication;
import com.chuckfacts.bean.JokeBean;
import com.example.chuckfacts.R;

public class FavouriteJokeAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ViewHolder holder;

	public FavouriteJokeAdapter(Context mContext) {
		this.mContext = mContext;
		this.inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return MyApplication.getInstance().favArrlist.size();
	}

	@Override
	public Object getItem(int position) {
		return MyApplication.getInstance().favArrlist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.jokerow, parent, false);
			holder = new ViewHolder();
			holder.txtJoke = (TextView) convertView
					.findViewById(R.id.text_joke);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		JokeBean bean = MyApplication.getInstance().favArrlist.get(position);

		if (bean.isFavourite()) {
			holder.txtJoke.setText(bean.getJoke());
			holder.txtJoke.setVisibility(View.VISIBLE);
		} else {
			holder.txtJoke.setVisibility(View.GONE);
		}

		convertView.setBackgroundColor(Color.WHITE);
		return convertView;
	}

	public static class ViewHolder {
		TextView txtJoke;
	}

}