package com.chuckfacts.listAdapter;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.chuckfacts.MyApplication;
import com.chuckfacts.bean.JokeBean;
import com.example.chuckfacts.R;

public class JokeListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ViewHolder holder;

	public JokeListAdapter(Context mContext) {
		this.mContext = mContext;
		this.inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return MyApplication.getInstance().mArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return MyApplication.getInstance().mArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.jokerow, parent, false);
			holder = new ViewHolder();
			holder.txtJoke = (TextView) convertView
					.findViewById(R.id.text_joke);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		JokeBean bean = MyApplication.getInstance().mArrayList.get(position);

		holder.txtJoke.setText(bean.getJoke());
		if (bean.isFavourite())
			convertView.setBackgroundColor(Color.parseColor("#C0C0C0"));
		else
			convertView.setBackgroundColor(Color.WHITE);

		convertView.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				JokeBean bean = MyApplication.getInstance().mArrayList.get(position);
				if (bean.isFavourite()) {
					bean.setFavourite(false);
					v.setBackgroundColor(Color.WHITE);
					Toast.makeText(mContext, "Unfavorite", Toast.LENGTH_SHORT)
							.show();
				} else {
					bean.setFavourite(true);
					v.setBackgroundColor(Color.parseColor("#C0C0C0"));
					Toast.makeText(mContext, "Favorite", Toast.LENGTH_SHORT)
							.show();
				}
				return false;
			}
		});
		return convertView;
	}

	public static class ViewHolder {
		TextView txtJoke;
	}
}
