package com.chuckfacts.callback;

import org.json.JSONArray;

public interface OnTaskCompletedListener {
	public void ontaskCompleted();
}
